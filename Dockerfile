FROM python:alpine
COPY . /app
WORKDIR /app
RUN pip install --cache-dir /app/pip-cache -r requirements.txt
RUN rm -r /app/pip-cache
ENTRYPOINT [ "python3", "manage.py", "runserver" ]
